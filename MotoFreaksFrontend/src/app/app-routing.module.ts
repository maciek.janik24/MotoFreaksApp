import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppRoute} from "./shared/enums/app-route.enum";
import {AppPath} from "./shared/enums/app-path.enum";
import {ConnectionRefusedComponent} from "./modules/common/connection-refused/connection-refused.component";
import {KcGuard} from "./shared/guards/kc-guard.guard";
import {RolesKc} from "./shared/enums/roles.enum";
import {LoggedHomeGuard} from "./shared/guards/logged-home.guard";


const routes: Routes = [
  {
    path: AppRoute.AUTH,
    loadChildren: () => import('./modules/authentication/authentication.module').then(m => m.AuthenticationModule),
  },
  {
    path: AppRoute.DASHBOARD,
    canActivate: [KcGuard],
    loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: AppRoute.PROFILE,
    canActivate: [KcGuard],
    loadChildren: () => import('./modules/profiles/profile.module').then(m => m.ProfileModule)
  },
  {
    path: AppRoute.USER,
    canActivate: [KcGuard],
    loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule)
  },
  {
    path: AppRoute.MESSAGE,
    canActivate: [KcGuard],
    loadChildren: () => import('./modules/messages/messages.module').then(m => m.MessagesModule)
  },
  {
    path: AppRoute.POSTS,
    canActivate: [KcGuard],
    loadChildren: () => import('./modules/posts/posts.module').then(m => m.PostsModule)
  },
  {
    path: AppRoute.CHALLENGES,
    canActivate: [KcGuard],
    loadChildren: () => import('./modules/challenges/challenges.module').then(m => m.ChallengesModule)
  },
  {
    path: AppRoute.CARS,
    canActivate: [KcGuard],
    data: {roles: [RolesKc.MODERATOR]},
    loadChildren: () => import('./modules/cars/cars.module').then(m => m.CarsModule)
  },
  {
    path: AppRoute.SENTENCES,
    canActivate: [KcGuard],
    loadChildren: () => import('./modules/sentences/sentences.module').then(m => m.SentencesModule)
  },
  {
    path: AppRoute.HOME,
    canActivate: [LoggedHomeGuard],
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
  },
  {
    path: AppRoute.SERVICE_NOT_AVAILABLE,
    component: ConnectionRefusedComponent,
  },
  {
    path: '',
    redirectTo: AppPath.HOME_PATH
  },
  {
    path: '**',
    redirectTo: AppPath.HOME_PATH
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
