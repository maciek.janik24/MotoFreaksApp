import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ChallengeDtoModel} from "../../logic/dto/response/challenge-dto.model";

@Component({
  selector: 'app-challenges-list',
  templateUrl: './challenges-list.component.html',
  styleUrls: ['./challenges-list.component.scss']
})
export class ChallengesListComponent implements OnInit {

  @Input()
  challengesList: ChallengeDtoModel[];
  @Input()
  isAdmin: boolean;
  @Input()
  isModerator: boolean;
  @Input()
  myId: string;
  @Output()
  deleteEvent = new EventEmitter<string>();
  @Output()
  mergeEvent = new EventEmitter<string>();


  constructor() {
  }

  ngOnInit(): void {
  }

  deleteChallenge($event: string) {
    this.deleteEvent.emit($event)
  }

  mergeChallenge($event: string) {
    this.mergeEvent.emit($event)
  }
}
