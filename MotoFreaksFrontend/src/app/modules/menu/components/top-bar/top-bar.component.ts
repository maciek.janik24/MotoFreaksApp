import {Component, OnInit} from '@angular/core';
import {KeycloakService} from "keycloak-angular";

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  isLogged: boolean;

  constructor(private keycloak: KeycloakService) {
  }

  ngOnInit(): void {
    this.keycloak.isLoggedIn().then((loggedState)=> this.isLogged=loggedState)
  }

}
