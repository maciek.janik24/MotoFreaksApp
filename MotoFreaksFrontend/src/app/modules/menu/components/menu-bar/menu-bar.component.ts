import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AppPath} from "../../../../shared/enums/app-path.enum";
import {MenuService} from "../../logic/services/menu.service";
import {Store} from "@ngrx/store";
import {AuthenticationState} from 'src/app/modules/authentication/logic/reducers/authentication.reducers';
import {AuthService} from "../../../authentication/logic/services/auth.service";
import {KeycloakService} from "keycloak-angular";
import {RolesKc} from "../../../../shared/enums/roles.enum";

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.scss']
})
export class MenuBarComponent implements OnInit {

  activeRoute: string = '';
  enabled: string = 'true';
  isLogged: boolean;
  isValidated: boolean;
  isModerator: boolean = false;


  constructor(private router: Router, private changeDetector: ChangeDetectorRef, private menuService: MenuService,
              private authStore: Store<AuthenticationState>, private authService: AuthService,
              private keycloak: KeycloakService) {
  }

  ngOnInit() {
    this.keycloak.isLoggedIn().then((loggedState) => {
      this.isLogged = loggedState
      if (loggedState){
        this.initMenu();
      }
    })
  }


  goToHome() {
    this.router.navigate([AppPath.DASHBOARD_PATH]);
  }

  logOut() {
    this.authService.logout()
  }

  goToMeProfile() {
    this.router.navigate([AppPath.PROFILE_ME_PATH])
  }

  goToAllUsers() {
    this.router.navigate([AppPath.USER_ALL])
  }

  goToPosts() {
    this.router.navigate([AppPath.POSTS_ALL_PATH])
  }

  goToChallenges() {
    this.router.navigate([AppPath.CHALLENGES_ALL_PATH])
  }

  goToModifyCars() {
    this.router.navigate([AppPath.MODIFY_CARS])
  }

  goToSentences() {
    this.router.navigate([AppPath.SENTENCES_ALL])
  }


  private initMenu(){
    this.authService.getValidation();
    this.authService.isValidatedUser().subscribe(validation => this.isValidated = validation)

    this.isModerator = this.keycloak.getUserRoles().includes(RolesKc.MODERATOR)


    this.menuService.activeRoute.subscribe(activeElement => {
      this.activeRoute = activeElement;
      this.changeDetector.detectChanges();
    })
    this.menuService.enabled.subscribe(enabled => {
      this.enabled = enabled;
      this.changeDetector.detectChanges();
    })
  }
}
