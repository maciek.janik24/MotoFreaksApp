import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {DashboardPageComponent} from "./pages/dashboard-page/dashboard-page.component";

export const routes: Routes = [
  {
    path: '',
    component: DashboardPageComponent
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
})
export class DashboardRoutingModule {
}
