import {Component, OnInit} from '@angular/core';
import {MenuService} from "../../../menu/logic/services/menu.service";
import {AuthService} from "../../../authentication/logic/services/auth.service";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {Observable} from "rxjs";
import {PostModel} from "../../../posts/logic/dto/model/post.model";
import {PostsService} from "../../../posts/logic/services/posts.service";
import {ActiveRoute} from "../../../../shared/enums/active-route.enum";
import {NotValidateDialogComponent} from "../../components/not-validate-dialog/not-validate-dialog.component";
import {AppPath} from "../../../../shared/enums/app-path.enum";

@Component({
  selector: 'app-home-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  postsListObs: Observable<PostModel[]>


  constructor(private menuService: MenuService, private authService: AuthService,
              private router:Router, public dialog: MatDialog,
              private postsService:PostsService) {

  }

  ngOnInit(): void {
    this.postsListObs = this.postsService.getAllPosts();
    this.menuService.activeRoute.next(ActiveRoute.DASHBOARD);
    this.authService.isValidatedUser().subscribe(validation=>{
      if(!validation && this.authService.isLoggedUser()){
        const dialogRef = this.dialog.open(NotValidateDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
          if(result){
            this.router.navigate([AppPath.PROFILE_ME_PATH]);
          }else{
            this.authService.logout()
          }
        });
      }
    });
  }

}
