import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardPageComponent} from "./pages/dashboard-page/dashboard-page.component";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {NotValidateDialogComponent} from './components/not-validate-dialog/not-validate-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";
import {LastPostComponent} from './components/last-post/last-post.component';
import {PostsModule} from "../posts/posts.module";
import {PostsService} from "../posts/logic/services/posts.service";


@NgModule({
  declarations: [DashboardPageComponent, NotValidateDialogComponent, LastPostComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatDialogModule,
    MatButtonModule,
    PostsModule,
  ],
  exports: [
    DashboardPageComponent
  ],
  providers:[
    PostsService,
  ]
})
export class DashboardModule {
}
