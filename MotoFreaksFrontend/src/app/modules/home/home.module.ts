import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {HomeRoutingModule} from "./home-routing.module";
import {AngularSvgIconModule} from "angular-svg-icon";


@NgModule({
  declarations: [
    HomePageComponent
  ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        AngularSvgIconModule
    ]
})
export class HomeModule { }
