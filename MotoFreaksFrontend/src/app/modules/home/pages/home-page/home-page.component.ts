import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AppPath} from "../../../../shared/enums/app-path.enum";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(private router: Router ) { }

  ngOnInit(): void {
  }

  goToApp() {
    this.router.navigate([AppPath.DASHBOARD_PATH]);
  }

  goToRegister() {
    this.router.navigate([AppPath.AUTH_REGISTER_PATH]);
  }
}
