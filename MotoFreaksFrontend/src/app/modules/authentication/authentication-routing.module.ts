import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegisterPageComponent} from './pages/register-page/register-page.component';
import {AppRoute} from "../../shared/enums/app-route.enum";

export const routes: Routes = [
  {
    path: AppRoute.REGISTER,
    component: RegisterPageComponent
  },

];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
})
export class AuthenticationRoutingModule {
}
