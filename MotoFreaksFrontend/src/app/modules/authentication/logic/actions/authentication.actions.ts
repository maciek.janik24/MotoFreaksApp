import {Action} from "@ngrx/store";
import {RegisterModel} from "../dto/request/register.model";


export const USER_REGISTER = '[Auth] USER_REGISTER';
export const USER_REGISTER_SUCCESS = '[Auth] USER_REGISTER_SUCCESS';
export const USER_REGISTER_FAIL = '[Auth] USER_REGISTER_FAILED';

export const USER_LOGOUT = '[Auth] USER_LOGOUT';

export const CHECK_VALIDATION = '[Auth] CHECK_VALIDATION';
export const CHECK_VALIDATION_SUCCESS = '[Auth] CHECK_VALIDATION_SUCCESS';
export const CHECK_VALIDATION_FAIL = '[Auth] CHECK_VALIDATION_FAIL';




export class UserRegister implements Action {
  readonly type = USER_REGISTER;

  constructor(public payload: RegisterModel) {
  }
}

export class UserRegisterSuccess implements Action {
  readonly type = USER_REGISTER_SUCCESS;

  constructor(public payload: string) {
  }
}

export class UserRegisterFail implements Action {
  readonly type = USER_REGISTER_FAIL;

  constructor(public payload: string) {
  }
}

export class UserLogout implements Action {
  readonly type = USER_LOGOUT;
}

export class CheckValidation implements Action {
  readonly type = CHECK_VALIDATION;

  constructor() {
  }
}

export class CheckValidationSuccess implements Action {
  readonly type = CHECK_VALIDATION_SUCCESS;

  constructor(public validation: boolean) {
  }
}

export class CheckValidationFail implements Action {
  readonly type = CHECK_VALIDATION_FAIL;

  constructor(public payload: string) {
  }
}

export type Actions =
  | UserLogout
  | CheckValidation
  | CheckValidationSuccess
  | CheckValidationFail

