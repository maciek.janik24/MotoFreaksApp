import {
  CHECK_VALIDATION,
  CHECK_VALIDATION_FAIL,
  CHECK_VALIDATION_SUCCESS,
  USER_LOGOUT
} from "../actions/authentication.actions";
import {createFeatureSelector, createSelector} from "@ngrx/store";

export interface AuthenticationState {
  isValidated:boolean;
  loading: boolean;
}

const INITIAL_STATE: AuthenticationState = {
  isValidated:true,
  loading:false
};

export function reducer(state: AuthenticationState = INITIAL_STATE, action) {
  switch (action.type) {
    case CHECK_VALIDATION:
      return {
        ...state,
        loading:true,
      };

    case CHECK_VALIDATION_SUCCESS:
      return {
        ...state,
        isValidated: action.validation
      }
    case CHECK_VALIDATION_FAIL:
      return state;

     case USER_LOGOUT:
      return INITIAL_STATE;

    default:
      return state;
  }

}

export const getUserValidation = (state) => state.isValidated;
export const getLoading = (state) => state.loading;


const fromAuthenticationState = createFeatureSelector<AuthenticationState>('authentication');

export const isValidated = createSelector(fromAuthenticationState, getUserValidation);
export const isLoading = createSelector(fromAuthenticationState, getLoading);
