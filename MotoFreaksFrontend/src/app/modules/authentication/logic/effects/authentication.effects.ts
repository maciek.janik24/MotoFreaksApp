import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {AuthenticationService} from '../services/authentication.service';
import {Action, Store} from '@ngrx/store';
import {
  CHECK_VALIDATION,
  CheckValidationFail,
  CheckValidationSuccess,
  USER_LOGOUT,
  USER_REGISTER,
  UserRegister,
  UserRegisterFail,
  UserRegisterSuccess
} from '../actions/authentication.actions';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {AppPath} from "../../../../shared/enums/app-path.enum";
import {Router} from "@angular/router";
import {CommonComponentsService} from "../../../common/common.service";
import {AuthenticationState} from "../reducers/authentication.reducers";

@Injectable()
export class AuthenticationEffects {
  constructor(private actions$: Actions, private store$: Store<AuthenticationState>
    , private authService: AuthenticationService, private router: Router
    , private errorService: CommonComponentsService) {
  }

  @Effect({dispatch: false})
  public logout$: Observable<Action> = this.actions$
    .pipe(
      ofType(USER_LOGOUT),
      tap((user) => localStorage.removeItem('token'))
    );

  @Effect()
  register$: Observable<Action> = this.actions$
    .pipe(
      ofType(USER_REGISTER),
      switchMap((action: UserRegister) => {
        return this.authService.register(action.payload);
      }),
      tap(() => {
        this.router.navigate([AppPath.DASHBOARD_PATH])
      }),
      switchMap((response: string) => [
        new UserRegisterSuccess(response),
      ]),
      catchError((error, caught) => {
        this.store$.dispatch(new UserRegisterFail(error.error));
        this.errorService.error(error);
        return caught;
      })
    );

  @Effect()
  checkValidation$: Observable<Action> = this.actions$
    .pipe(
      ofType(CHECK_VALIDATION),
      switchMap(() => {
        return this.authService.checkUserValidation();
      }),
      switchMap((validation: boolean) => [
        new CheckValidationSuccess(validation)
      ]),
      catchError((error, caught) => {
        this.store$.dispatch(new CheckValidationFail(error.error));
        this.errorService.error(error);
        return caught;
      })
    )
}
