import {Store} from "@ngrx/store";
import {Injectable} from "@angular/core";
import {CheckValidation} from "../actions/authentication.actions";
import {AuthenticationState, isValidated} from "../reducers/authentication.reducers";
import {Observable, of, Subscription} from "rxjs";
import {KeycloakService} from "keycloak-angular";
import {RolesKc} from "../../../../shared/enums/roles.enum";
import {delay} from "rxjs/operators";
import {AppPath} from "../../../../shared/enums/app-path.enum";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {
  private isLogged: boolean;
  private tokenTimeSubscription = new Subscription();

  constructor(
    protected readonly router: Router,
    private store: Store<AuthenticationState>,
    private keycloak: KeycloakService) {
    this.keycloak.isLoggedIn().then((loggedState) => this.isLogged = loggedState)

  }

  isLoggedUser() {
    return this.isLogged;
  }

  isValidatedUser(): Observable<boolean> {
    return this.store.select(isValidated);
  }

  getValidation() {
    this.store.dispatch(new CheckValidation())
  }

  isAdmin() {
    return this.keycloak.getUserRoles().includes(RolesKc.ADMIN);
  }

  isModerator() {
    return this.keycloak.getUserRoles().includes(RolesKc.MODERATOR);
  }

  isUser() {
    return this.keycloak.getUserRoles().includes(RolesKc.USER);
  }

  logout() {
    this.tokenTimeSubscription.unsubscribe();
    this.keycloak.logout('http://localhost:4200/home')
  }

  setTokenTime(time) {
    this.tokenTimeSubscription.unsubscribe();
    this.tokenTimeSubscription = of(null).pipe(
      delay(time)
    ).subscribe(() => {
      console.log('Your token was expired... ')
      this.router.navigate([AppPath.DASHBOARD_PATH])
      this.logout();
    })
  }
}
