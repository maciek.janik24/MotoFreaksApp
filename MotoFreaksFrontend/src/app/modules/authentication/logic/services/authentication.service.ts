import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {RegisterModel} from "../dto/request/register.model";

@Injectable()
export class AuthenticationService {
  constructor(private readonly httpClient: HttpClient) {
  }


  register(register: RegisterModel): Observable<any> {
    return this.httpClient.put(`${environment.apiUrl}/auth/kc/register`,
      {
        username: register.username,
        password: register.password,
        name: register.name,
        lastName: register.lastName,
        email: register.email
      });
  }

checkUserValidation():Observable<boolean>{
  return this.httpClient.get<boolean>(`${environment.apiUrl}/account/validation`)
}
}
