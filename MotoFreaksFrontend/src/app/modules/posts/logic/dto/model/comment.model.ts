import {UserBasicInfoModel} from "../../../../users/logic/dto/user.types";

export class CommentModel {

  id:string;
  content:string;
  creatorId:string;
  createdDate: Date;
  creatorData: UserBasicInfoModel;
  approved: string[];
  rejected: string[];


  constructor(id: string, content: string, creatorId: string, createdDate: Date, creatorData: UserBasicInfoModel, approved: string[], rejected: string[]) {
    this.id = id;
    this.content = content;
    this.creatorId = creatorId;
    this.createdDate = createdDate;
    this.creatorData = creatorData;
    this.approved = approved;
    this.rejected = rejected;
  }
}
