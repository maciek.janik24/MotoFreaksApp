import {Component, Input, OnInit} from '@angular/core';
import {MessageDataModel} from "../../logic/dto/response/message-data.model";
import {AppPath} from "../../../../shared/enums/app-path.enum";
import {Router} from "@angular/router";
import {UsersService} from "../../../users/logic/services/users.service";
import {Observable} from "rxjs";
import {MessagesService} from "../../logic/services/messages.service";

@Component({
  selector: 'app-conv-list',
  templateUrl: './conv-list.component.html',
  styleUrls: ['./conv-list.component.scss']
})
export class ConvListComponent implements OnInit {

  @Input()
  messages$: Observable<MessageDataModel[]>;
  loading: Observable<boolean>

  constructor(private router: Router, private userService: UsersService, private messagesService: MessagesService) {
    this.loading=this.messagesService.isLoading();
  }

  ngOnInit(): void {
  }

  goToExpandConv(id: string) {
    this.router.navigate([AppPath.MESSAGE_CONVERSATION, {id: id}])
  }

}
