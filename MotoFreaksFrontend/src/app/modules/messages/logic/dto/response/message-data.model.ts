import {MessageModel} from "../model/message.model";
import {UserBasicInfoModel} from "../../../../users/logic/dto/user.types";

export class MessageDataModel {
  receiverId: string;
  receiverData: UserBasicInfoModel;
  lastMessageDate: Date
  lastMessage: MessageModel;


  constructor(receiverId: string, receiverData: UserBasicInfoModel, lastMessage: Date, messages: MessageModel) {
    this.receiverId = receiverId;
    this.receiverData = receiverData;
    this.lastMessageDate = new Date(lastMessage);
    this.lastMessage = messages;
  }
}
