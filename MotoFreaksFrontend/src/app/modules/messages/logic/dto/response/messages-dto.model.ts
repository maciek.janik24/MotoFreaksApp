import {UserBasicInfoModel} from "../../../../users/logic/dto/user.types";
import {MessageModel} from "../model/message.model";

export class MessagesDto {
  messages: MessageModel[];
  receiverData: UserBasicInfoModel;


  constructor(messages: MessageModel[],receiverData: UserBasicInfoModel) {
    this.messages = messages;
    this.receiverData= receiverData;
  }
}
