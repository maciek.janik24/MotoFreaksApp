import {KeycloakService} from "keycloak-angular";

export function initializer(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: {
            url: 'http://localhost:1506' ,
            realm: 'motofreaks',
            clientId: 'ui-mtfr',

          },
          loadUserProfileAtStartUp:true,
          initOptions: {
            onLoad: 'check-sso',
            checkLoginIframe: false,
          },
          bearerExcludedUrls: ['/assets', '/auth/register','/service-not-available'],
        });
        resolve(resolve);
      } catch (error) {
        reject(error);
      }
    });
  };
}
