import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {KeycloakAuthGuard, KeycloakService} from "keycloak-angular";
import {AppPath} from "../enums/app-path.enum";

@Injectable({
  providedIn: 'root'
})
export class LoggedHomeGuard extends KeycloakAuthGuard {
  constructor(
    protected readonly router: Router,
    protected readonly keycloak: KeycloakService
  ) {
    super(router, keycloak);
  }

  public async isAccessAllowed(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    if (this.authenticated) {
      this.router.navigate([AppPath.DASHBOARD_PATH])
      return false;
    } else {
      return true;
    }
  }
}
