export enum ActiveRoute {

  REGISTER = '[Active] REGISTER',
  DASHBOARD = '[Active] DASHBOARD',
  MY_PROFILE = '[Active] MY_PROFILE',
  ALL_USERS = '[Active] ALL_USERS',
  POSTS = '[Active] POSTS',
  CHALLENGE = '[Active] CHALLENGE',
  CARS = '[Active] CARS',
  SENTENCES = '[Active] SENTENCES',
}
