export enum AppRoute {

  //common routes
  ALL = 'all',
  CREATE = 'create',
  SERVICE_NOT_AVAILABLE = 'service-not-available',

  //home routes
  HOME='home',


  //auth routes
  AUTH = 'auth',
  REGISTER = 'register',

  DASHBOARD = 'dashboard',

  //profiles routes
  PROFILE = 'profile',
  ME = 'me',
  USER = 'user',

  //messages routes
  MESSAGE = 'messages',
  CONVERSATION = 'conversation',

  //posts routes
  POSTS = 'posts',


//challenges routes
  CHALLENGES = 'challenges',
  FILL = 'fill',
  MERGE='merge',

  //carsCompany
  CARS='cars',
  MODIFY='modify',


  //sentences
  SENTENCES='sentences'
}
