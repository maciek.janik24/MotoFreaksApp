export enum AppPath {
  AUTH_REGISTER_PATH = '/auth/register',

  HOME_PATH = '/home',

  DASHBOARD_PATH = '/dashboard',

  PROFILE_ME_PATH = '/profile/me',
  PROFILE_USER_PATH = '/profile/user',
  USER_ALL = '/user/all',

  MESSAGE_ALL = '/messages/all',
  MESSAGE_CONVERSATION = '/messages/conversation',

  POSTS_ALL_PATH = '/posts/all',
  POSTS_CREATE_PATH = '/posts/create',

  CHALLENGES_ALL_PATH = '/challenges/all',
  CHALLENGES_DO_PATH = '/challenges/fill',
  CHALLENGE_CREATE = '/challenges/create',
  CHALLENGE_MERGE = '/challenges/merge',

  SERVICE_NOT_AVAILABLE = '/service-not-available',

  MODIFY_CARS='/cars/modify',

  SENTENCES_ALL='/sentences/all'
}
