export const environment = {
  production: false,
  apiUrl: 'http://localhost:8528',
  jwtExpirationTime: 3600,
  localstorageSessionKey: 'sessionMTFR'
};

