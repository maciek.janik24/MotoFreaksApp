package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.controller;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.NewMessage;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/message")
@Slf4j
@CrossOrigin("*")
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping(path = "/send/{receiverId}", method = RequestMethod.PUT, produces = "application/json")
    public Object sendMessage(@RequestBody NewMessage messageContent, @PathVariable String receiverId) {
        return messageService.sendMessage(receiverId, messageContent);
    }

    @RequestMapping(path = "/read/{receiverId}", method = RequestMethod.POST, produces = "application/json")
    public Object getReadAllMessagesWithUser(@PathVariable String receiverId) {
        return messageService.readMessage(receiverId);
    }

    @RequestMapping(path = "/unread", method = RequestMethod.GET, produces = "application/json")
    public Object getUnreadCount() {
        return messageService.getUnreadMessage();
    }

    @RequestMapping(path = "/chats", method = RequestMethod.GET, produces = "application/json")
    public Object getChats() {
        return messageService.getChatsInfo();
    }

    @RequestMapping(path = "/{receiverId}", method = RequestMethod.GET, produces = "application/json")
    public Object getMessages(@PathVariable String receiverId) {
        return messageService.getMessages(receiverId);
    }
}
