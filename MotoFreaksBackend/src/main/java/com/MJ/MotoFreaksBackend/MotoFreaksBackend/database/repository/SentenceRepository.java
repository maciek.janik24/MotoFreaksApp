package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository;


import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Sentence;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SentenceRepository extends MongoRepository<Sentence, String> {
}
