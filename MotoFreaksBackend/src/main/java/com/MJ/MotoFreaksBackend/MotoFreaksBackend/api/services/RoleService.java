package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.UserRoles;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository.RoleRepository;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
@Deprecated
public class RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public UserRoles getRoleByName(Role role) {
        Optional<UserRoles> optionalRole = roleRepository.findByRole(role);
        return optionalRole.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Role not found"));
    }
}
