package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.initialization.executor;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.AuthAccountCreateModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services.KeycloakApiService;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.initialization.initData.*;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository.*;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.RoleKc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Objects;

@Component
@Slf4j
public class DBLoader implements CommandLineRunner {

    @Value("${api.keycloak.user.init.password}")
    String initPassword;

    private final AccountRepository accountRepository;
    private final CarCompanyRepository carCompanyRepository;
    private final ChallengeRepository challengeRepository;
    private final SentenceRepository sentenceRepository;
    private final PostsRepository postsRepository;
    private final KeycloakApiService keycloakApiService;


    public DBLoader(
            AccountRepository accountRepository,
            CarCompanyRepository carCompanyRepository,
            ChallengeRepository challengeRepository,
            SentenceRepository sentenceRepository,
            PostsRepository postsRepository, KeycloakApiService keycloakApiService) {
        this.accountRepository = accountRepository;
        this.carCompanyRepository = carCompanyRepository;
        this.challengeRepository = challengeRepository;
        this.sentenceRepository = sentenceRepository;
        this.postsRepository = postsRepository;
        this.keycloakApiService = keycloakApiService;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Starting loading init data ... ");
        Date nowDate = new Date();


        //TODO  add account and KC user creation

        AccountLoader accountLoader = new AccountLoader(nowDate);
        if (this.accountRepository.count() == 0 ||
                (this.accountRepository.count() == accountLoader.accountToLoad.size()
                        && this.accountRepository.findAll().stream().noneMatch(entity -> Objects.nonNull(entity.getUpdatedDate())))) {
            log.info("Loading ADMIN account...");
            this.accountRepository.deleteAll();
            this.accountRepository.saveAll(accountLoader.accountToLoad);
            accountLoader.accountToLoad.forEach(account -> {
                AuthAccountCreateModel authAccountCreateModel = new AuthAccountCreateModel(
                        account.getUsername(),
                        account.getContact().getEmail(),
                        initPassword,
                        account.getName(),
                        account.getLastName()
                );
                try {
                    this.keycloakApiService.saveNewUserInKC(authAccountCreateModel, RoleKc.ADMIN, account.getId());
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                }

            });
        }


        //Cars Companies
        CarsLoader carsLoader = new CarsLoader(nowDate);
        if (this.carCompanyRepository.count() == 0 ||
                (this.carCompanyRepository.count() == carsLoader.carsToLoad.size()
                        && this.carCompanyRepository.findAll().stream().noneMatch(entity -> Objects.nonNull(entity.getUpdatedDate())))
        ) {
            log.info("Loading car companies...");
            this.carCompanyRepository.deleteAll();
            this.carCompanyRepository.saveAll(carsLoader.carsToLoad);
        }


        //Challenges
        QuizLoader quizLoader = new QuizLoader(nowDate);
        if (this.challengeRepository.count() == 0 ||
                (this.challengeRepository.count() == quizLoader.quizToLoad.size()
                        && this.challengeRepository.findAll().stream().noneMatch(entity -> Objects.nonNull(entity.getUpdatedDate())))
        ) {
            log.info("Loading challenges...");
            this.challengeRepository.deleteAll();
            this.challengeRepository.saveAll(quizLoader.quizToLoad);
        }


        //Sentence
        SentenceLoader sentenceLoader = new SentenceLoader(nowDate);
        if (this.sentenceRepository.count() == 0 ||
                (this.sentenceRepository.count() == sentenceLoader.sentenceToLoads.size()
                        && this.sentenceRepository.findAll().stream().noneMatch(entity -> Objects.nonNull(entity.getUpdatedDate())))
        ) {
            log.info("Loading sentences...");
            this.sentenceRepository.deleteAll();
            this.sentenceRepository.saveAll(sentenceLoader.sentenceToLoads);
        }

        //Posts
        PostLoader postLoader = new PostLoader(nowDate);
        if (this.postsRepository.count() == 0 || this.postsRepository.count() == postLoader.postsToload.size()) {
            //TODO dodać warunek czy istniejacy post ma tytul mojego postu
            log.info("Loading posts...");
            this.postsRepository.deleteAll();
            this.postsRepository.saveAll(postLoader.postsToload);
        }


        log.info("Finished loading data.");
    }
}
