package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.controller;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.NewChallengeModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services.ChallengeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/challenge")
@CrossOrigin("*")
public class ChallengeController {

    private final ChallengeService challengeService;

    @Autowired
    public ChallengeController(ChallengeService challengeService) {
        this.challengeService = challengeService;
    }

    @RequestMapping(path = "", method = RequestMethod.PUT, produces = "application/json")
    public Object createChallenge(@RequestBody NewChallengeModel challenge) {
        return challengeService.createChallenge(challenge);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.POST, produces = "application/json")
    public Object mergeChallenge(@PathVariable String id, @RequestBody NewChallengeModel challenge) {
        return challengeService.mergeChallenge(id, challenge);
    }

  @RequestMapping(path = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public Object deleteChallenge(@PathVariable String id) {
        return challengeService.deleteChallenge(id);
    }

    @RequestMapping(path = "/{challengeId}/competitor/points/{points}", method = RequestMethod.POST, produces = "application/json")
    public Object addCompetitor(@PathVariable String challengeId, @PathVariable int points) {
        return challengeService.addCompetitor(challengeId, points);
    }

    @RequestMapping(path = "", method = RequestMethod.GET, produces = "application/json")
    public Object findAll(@RequestParam Map<String, String> reqParams) {
        return challengeService.getAll(false,reqParams);
    }

    @RequestMapping(path = "/general", method = RequestMethod.GET, produces = "application/json")
    public Object findAllGeneral(@RequestParam Map<String, String> reqParams) {
        return challengeService.getAll(true, reqParams);
    }

    @RequestMapping(path = "/byUser/{id}", method = RequestMethod.GET, produces = "application/json")
    public Object findByUser(@PathVariable String id) {
        return challengeService.findByUser(id);
    }

    @RequestMapping(path = "/{challengeId}/questions", method = RequestMethod.GET, produces = "application/json")
    public Object getQuestionsById(@PathVariable String challengeId) {
        return challengeService.getQuestions(challengeId);
    }

    @RequestMapping(path = "/exists/{name}", method = RequestMethod.GET, produces = "application/json")
    public Object isExists(@PathVariable String name) {
        return challengeService.isExistByName(name);
    }
}
