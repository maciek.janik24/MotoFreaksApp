package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.UserBasicInfoModel;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ChallengeDto {

    private String id;
    private String name;
    private String company;
    private String model;
    private String generation;
    private String creatorId;
    private boolean general;
    private boolean alreadyFilled;
    private int obtainPoints;
    private int questionsCount;
    private int allPoints;
    private UserBasicInfoModel creatorData;
}
