package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.CarDataModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Contact;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class MyAccountDto {

    private String id;
    private String username;
    private String name;
    private String lastName;
    private Gender gender;
    private boolean enabled;
    private Date createdDate;
    private Date updatedDate;
    private List<Date> loginsHistory;
    private List<CarDataModel> carsList;
    private Contact contact;
    private Address address;
    private Integer points;
}
