package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums;

public enum RoleKc {
    USER("user_mtfr"), MODERATOR("moderator_mtfr"), ADMIN("admin_mtfr");

    public final String name;

    RoleKc(String name) {
        this.name = name;
    }
}
