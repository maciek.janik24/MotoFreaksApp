package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums;

public enum MemberState {
    INTERESTED,
    GOING,
    REJECTED,
    ACCEPTED

}
