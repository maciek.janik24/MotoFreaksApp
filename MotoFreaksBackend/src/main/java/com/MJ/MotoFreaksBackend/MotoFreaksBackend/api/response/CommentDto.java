package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.UserBasicInfoModel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Data
public class CommentDto {

    private String id;
    private String content;
    private String creatorId;
    private Date createdDate;
    private UserBasicInfoModel creatorData;
    private List<String> approved;
    private List<String> rejected;
}
