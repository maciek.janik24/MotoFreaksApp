package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.Gender;
import lombok.Data;

@Data
@Deprecated
public class MergeUser {

    private String name;
    private String lastName;
    private Gender gender;
    private boolean enabled;
    private String password;
}
