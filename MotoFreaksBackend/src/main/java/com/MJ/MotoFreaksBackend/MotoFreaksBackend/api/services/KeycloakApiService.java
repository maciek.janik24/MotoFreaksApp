package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.AuthAccountCreateModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.KCCredentialUserModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.KCUserModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.RoleKc;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.UserGroupKc;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.MJ.MotoFreaksBackend.MotoFreaksBackend.config.consts.AuthKcConsts.*;
import static org.springframework.http.ResponseEntity.ok;

@Service
@Slf4j
public class KeycloakApiService {

    @Value("${keycloak.auth-server-url}")
    public String kcUrl;
    @Value("${api.keycloak.client.id}")
    public String apiClientId;
    @Value("${api.keycloak.client.secret}")
    public String apiClientSecret;
    @Value("${api.keycloak.grand.type}")
    public String apiGrandType;
    @Value("${api.keycloak.token.path}")
    public String apiTokenPath;
    @Value("${api.keycloak.user.path}")
    public String apiUserPath;
    @Autowired
    private AccountService accountService;


    public Object addUser(AuthAccountCreateModel authAccountCreateModel) {
        return addUser(authAccountCreateModel, RoleKc.USER);
    }

    public Object addUser(AuthAccountCreateModel authAccountCreateModel, RoleKc roleKc) {
        Map<Object, Object> model = new HashMap<>();
        String createdAccountId = null;
        try {
            accountService.getAccountByUserName(authAccountCreateModel.getUsername());
            log.warn("Cannot register user: " + authAccountCreateModel.getUsername() + ". User is already exists");
            return new ResponseEntity<Object>("User  '" + authAccountCreateModel.getUsername() + "' is already exists!", HttpStatus.NOT_FOUND);
        } catch (ResponseStatusException e) {
            try {
                createdAccountId = accountService.addNewAccount(authAccountCreateModel).getId();
                saveNewUserInKC(authAccountCreateModel, roleKc, createdAccountId);
                model.put("message", "User registered successfully");
                log.info("User " + authAccountCreateModel.getUsername() + " was register correctly.");
                return ok(model);
            } catch (Exception ex) {
                if (createdAccountId != null) {
                    accountService.deleteAccount(createdAccountId);
                }
                return new ResponseEntity<Object>("Something goes wrong!", HttpStatus.BAD_REQUEST);
            }
        }
    }


    public void saveNewUserInKC(AuthAccountCreateModel authAccountCreateModel, RoleKc roleKc, String accountId) {
        String token = getClientToken();
        String jsonBody = createJsonForUserCreation(authAccountCreateModel, roleKc, accountId);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);

        HttpEntity<String> request = new HttpEntity<>(jsonBody, headers);
        try {
            restTemplate.postForEntity(kcUrl + apiUserPath, request, String.class);
        } catch (HttpClientErrorException httpClientErrorException) {
            log.error("Keycloak request failed with: " +
                    httpClientErrorException.getRawStatusCode() + " code and message: " + httpClientErrorException.getResponseBodyAsString());
            throw new ResponseStatusException(httpClientErrorException.getStatusCode(), httpClientErrorException.getResponseBodyAsString());
        }
    }

    private String createJsonForUserCreation(AuthAccountCreateModel authAccountCreateModel, RoleKc roleKc, String accountId) {
        String jsonUserCreation = null;
        Map<String, List<String>> attributes = Stream.of(
                new AbstractMap.SimpleEntry<>(MTFR_ACCOUNT_KEY_NAME, Collections.singletonList(accountId))
        ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        KCUserModel newKcModel = new KCUserModel(
                authAccountCreateModel.getFirstname(),
                authAccountCreateModel.getLastName(),
                authAccountCreateModel.getEmail(),
                "true",
                authAccountCreateModel.getUsername(),
                attributes,
                Collections.singletonList(new KCCredentialUserModel(CREDENTIAL_TYPE, authAccountCreateModel.getPassword(), "false")),
                Collections.singletonList(mapRoleToGroup(roleKc).name),
                Collections.singletonList(OTP_ACTION)
        );

        try {
            jsonUserCreation = new ObjectMapper().writeValueAsString(newKcModel);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return jsonUserCreation;
    }

    private String getClientToken() {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_secret", apiClientSecret);
        map.add("client_id", apiClientId);
        map.add("grant_type", apiGrandType);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(kcUrl + apiTokenPath, request, String.class);

        try {
            JsonNode jsonResponse = new ObjectMapper().readTree(response.getBody());
            return jsonResponse.get("access_token").asText();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

    private UserGroupKc mapRoleToGroup(RoleKc roleKc){
        return switch (roleKc) {
            case MODERATOR -> UserGroupKc.MODERATOR_GROUP;
            case ADMIN -> UserGroupKc.ADMIN_GROUP;
            default -> UserGroupKc.USER_GROUP;
        };
    }
}
