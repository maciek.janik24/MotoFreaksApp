package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.initialization.initData;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Sentence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SentenceLoader {

    public List<Sentence> sentenceToLoads;


    public SentenceLoader(Date now){
        sentenceToLoads = new ArrayList<>();

        Sentence sentence1 = new Sentence();
        sentence1.setCreatedDate(now);
        sentence1.setUpdatedDate(null);
        sentence1.setName("LPG");
        sentence1.setTranslation("Liquified Petroleum Gas – oznaczenie mieszaniny gazów (propan + butan) używanych do napędzania silników spalinowych");
        sentence1.setCreatorId("sys");
        sentenceToLoads.add(sentence1);
        
        Sentence sentence2 = new Sentence();
        sentence2.setCreatedDate(now);
        sentence2.setUpdatedDate(null);
        sentence2.setName("SUV");
        sentence2.setTranslation("Sport Utilities Vehicle – pojazdy terenowe – wielozadaniowe");
        sentence2.setCreatorId("sys");
        sentenceToLoads.add(sentence2);
        
        Sentence sentence3 = new Sentence();
        sentence3.setCreatedDate(now);
        sentence3.setUpdatedDate(null);
        sentence3.setName("Choinka na desce");
        sentence3.setTranslation("Duża ilość kontrolek na desce rozdzielczej");
        sentence3.setCreatorId("sys");
        sentenceToLoads.add(sentence3);
        
        Sentence sentence4 = new Sentence();
        sentence4.setCreatedDate(now);
        sentence4.setUpdatedDate(null);
        sentence4.setName("Roadster");
        sentence4.setTranslation("Rodzaj otwartego nadwozia (cabrio) dwuosobowego samochodu sportowego");
        sentence4.setCreatorId("sys");
        sentenceToLoads.add(sentence4);
        
        Sentence sentence5 = new Sentence();
        sentence5.setCreatedDate(now);
        sentence5.setUpdatedDate(null);
        sentence5.setName("TCS");
        sentence5.setTranslation("Traction Control System - układ kontroli trakcji");
        sentence5.setCreatorId("sys");
        sentenceToLoads.add(sentence5);
        
        Sentence sentence6 = new Sentence();
        sentence6.setCreatedDate(now);
        sentence6.setUpdatedDate(null);
        sentence6.setName("DPF");
        sentence6.setTranslation("Filtr cząstek stałych w silnikach wysokoprężnych");
        sentence6.setCreatorId("sys");
        sentenceToLoads.add(sentence6);
        
        Sentence sentence7 = new Sentence();
        sentence7.setCreatedDate(now);
        sentence7.setUpdatedDate(null);
        sentence7.setName("Quattro");
        sentence7.setTranslation("Oznaczenie w marce Audi stałego napędu na cztery koła");
        sentence7.setCreatorId("sys");
        sentenceToLoads.add(sentence7);
        
        Sentence sentence8 = new Sentence();
        sentence8.setCreatedDate(now);
        sentence8.setUpdatedDate(null);
        sentence8.setName("ABS");
        sentence8.setTranslation("Anti-Lock Brakes System - układ stosowany w pojazdach mechanicznych celem zapobiegania blokowania blokowaniu kół podczas hamowania");
        sentence8.setCreatorId("sys");
        sentenceToLoads.add(sentence8);
        
        Sentence sentence9 = new Sentence();
        sentence9.setCreatedDate(now);
        sentence9.setUpdatedDate(null);
        sentence9.setName("AWD");
        sentence9.setTranslation("Z ang.: Ali Wheels Driven - napęd na wszystkie koła");
        sentence9.setCreatorId("sys");
        sentenceToLoads.add(sentence9);
        
        Sentence sentence10 = new Sentence();
        sentence10.setCreatedDate(now);
        sentence10.setUpdatedDate(null);
        sentence10.setName("Katalizator");
        sentence10.setTranslation("Jest urziądzeniem ograniczajiącym wydzielanie się do środowiska szkodliwych spalin z silnika samochodu");
        sentence10.setCreatorId("sys");
        sentenceToLoads.add(sentence10);
        
        Sentence sentence11 = new Sentence();
        sentence11.setCreatedDate(now);
        sentence11.setUpdatedDate(null);
        sentence11.setName("Wahacz");
        sentence11.setTranslation("Element zawieszenia samochodu łączący piastę koła (zwrotnicę) z nadwoziem");
        sentence11.setCreatorId("sys");
        sentenceToLoads.add(sentence11);
        
        Sentence sentence12 = new Sentence();
        sentence12.setCreatedDate(now);
        sentence12.setUpdatedDate(null);
        sentence12.setName("ECU");
        sentence12.setTranslation("Electronic Control Unit to komputer nadzorujący pracę danego podzespołu samochodu");
        sentence12.setCreatorId("sys");
        sentenceToLoads.add(sentence12);
        
        Sentence sentence13 = new Sentence();
        sentence13.setCreatedDate(now);
        sentence13.setUpdatedDate(null);
        sentence13.setName("A/C");
        sentence13.setTranslation("Air Condition – klimatyzacja");
        sentence13.setCreatorId("sys");
        sentenceToLoads.add(sentence13);
        
        Sentence sentence14 = new Sentence();
        sentence14.setCreatedDate(now);
        sentence14.setUpdatedDate(null);
        sentence14.setName("xDrive");
        sentence14.setTranslation("System napędu na cztery koła adaptacyjnie rozdzielający moc na obie osie. Nazwa stosowana w marce BMW");
        sentence14.setCreatorId("sys");
        sentenceToLoads.add(sentence14);
        
        Sentence sentence15 = new Sentence();
        sentence15.setCreatedDate(now);
        sentence15.setUpdatedDate(null);
        sentence15.setName("RWD");
        sentence15.setTranslation("Raer Wheel Drive – oznaczenie samochodu napędem na tylnie koła");
        sentence15.setCreatorId("sys");
        sentenceToLoads.add(sentence15);
        
        Sentence sentence16 = new Sentence();
        sentence16.setCreatedDate(now);
        sentence16.setUpdatedDate(null);
        sentence16.setName("Komputer pokładowy");
        sentence16.setTranslation("Urządzenie wyświetlające na monitorze informacje użyteczne kierowcy");
        sentence16.setCreatorId("sys");
        sentenceToLoads.add(sentence16);
        
        Sentence sentence17 = new Sentence();
        sentence17.setCreatedDate(now);
        sentence17.setUpdatedDate(null);
        sentence17.setName("Dać po heblach");
        sentence17.setTranslation("Bardzo mocno zahamować");
        sentence17.setCreatorId("sys");
        sentenceToLoads.add(sentence17);
        
        Sentence sentence18 = new Sentence();
        sentence18.setCreatedDate(now);
        sentence18.setUpdatedDate(null);
        sentence18.setName("OFF-ROAD");
        sentence18.setTranslation("określenie samochodów terenowych, zawodów w jeździe terenowej");
        sentence18.setCreatorId("sys");
        sentenceToLoads.add(sentence18);
        
        Sentence sentence19 = new Sentence();
        sentence19.setCreatedDate(now);
        sentence19.setUpdatedDate(null);
        sentence19.setName("4WD");
        sentence19.setTranslation("Oznaczenie napędu na obie osie");
        sentence19.setCreatorId("sys");
        sentenceToLoads.add(sentence19);
        
        Sentence sentence20 = new Sentence();
        sentence20.setCreatedDate(now);
        sentence20.setUpdatedDate(null);
        sentence20.setName("2WD");
        sentence20.setTranslation("Oznaczenie napędu na jedną oś np. dwa przednie koła");
        sentence20.setCreatorId("sys");
        sentenceToLoads.add(sentence20);
        
        Sentence sentence21 = new Sentence();
        sentence21.setCreatedDate(now);
        sentence21.setUpdatedDate(null);
        sentence21.setName("HUD");
        sentence21.setTranslation("To wyświetlacz samochodowy umieszczony na wysokości głowy");
        sentence21.setCreatorId("sys");
        sentenceToLoads.add(sentence21);
        
        Sentence sentence22 = new Sentence();
        sentence22.setCreatedDate(now);
        sentence22.setUpdatedDate(null);
        sentence22.setName("Gruz");
        sentence22.setTranslation("Samochód o złym stanie technicznym ");
        sentence22.setCreatorId("sys");
        sentenceToLoads.add(sentence22);
        
        Sentence sentence23 = new Sentence();
        sentence23.setCreatedDate(now);
        sentence23.setUpdatedDate(null);
        sentence23.setName("Coupe");
        sentence23.setTranslation("Rodzaj dwudrzwiowego nadwozia auta sportowego");
        sentence23.setCreatorId("sys");
        sentenceToLoads.add(sentence23);


    }
}
