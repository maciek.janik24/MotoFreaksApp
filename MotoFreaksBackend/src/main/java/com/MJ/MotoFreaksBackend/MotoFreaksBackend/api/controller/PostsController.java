package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.controller;


import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.NewMessage;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.NewPost;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services.PostsService;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.PostType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/posts")
@Slf4j
@CrossOrigin("*")
public class PostsController {

    private final PostsService postsService;

    @Autowired
    public PostsController(PostsService postsService) {
        this.postsService = postsService;
    }

    @RequestMapping(path = "/ALL", method = RequestMethod.GET, produces = "application/json")
    public Object getAll(@RequestParam Map<String, String> reqParams) {
        return postsService.getPosts(null,reqParams,null);
    }

    @RequestMapping(path = "/{type}", method = RequestMethod.GET, produces = "application/json")
    public Object getAllByType(@PathVariable PostType type, @RequestParam Map<String, String> reqParams) {
        return postsService.getPosts(type, reqParams,null);
    }
    @RequestMapping(path = "/byUser/{id}/ALL", method = RequestMethod.GET, produces = "application/json")
    public Object getPostsByUser(@PathVariable String id, @RequestParam Map<String, String> reqParams) {
        return postsService.getPosts(null, reqParams, id);
    }
    @RequestMapping(path = "/byUser/{id}/{type}", method = RequestMethod.GET, produces = "application/json")
    public Object getPostsByUserAndType(@PathVariable String id, @PathVariable PostType type, @RequestParam Map<String, String> reqParams) {
        return postsService.getPosts(type, reqParams, id);
    }

    @RequestMapping(path = "", method = RequestMethod.PUT, produces = "application/json")
    public Object addPost(@RequestBody NewPost newPost) {
        return postsService.addPost(newPost);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public Object deletePost(@PathVariable String id) {
        return postsService.deletePost(id);
    }

    @RequestMapping(path = "/resolve/{id}", method = RequestMethod.POST, produces = "application/json")
    public Object resolvePost(@PathVariable String id) {
        return postsService.resolvePost(id);
    }

    @RequestMapping(path = "/{postId}/comment", method = RequestMethod.PUT, produces = "application/json")
    public Object addComment(@PathVariable String postId, @RequestBody NewMessage newComment) {
        return postsService.addComment(postId, newComment);
    }

    @RequestMapping(path = "/{postId}/comment/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public Object deleteComment(@PathVariable String postId,  @PathVariable String id) {
        return postsService.deleteComment(postId, id);
    }

    @RequestMapping(path = "/{postId}/approve/comment/{id}", method = RequestMethod.POST, produces = "application/json")
    public Object approveComment(@PathVariable String postId,  @PathVariable String id) {
        return postsService.approveComment(postId, id);
    }

    @RequestMapping(path = "/{postId}/reject/comment/{id}", method = RequestMethod.POST, produces = "application/json")
    public Object rejectComment(@PathVariable String postId,  @PathVariable String id) {
        return postsService.rejectComment(postId, id);
    }
}
