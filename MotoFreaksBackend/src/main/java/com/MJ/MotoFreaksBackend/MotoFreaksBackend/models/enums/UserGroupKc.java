package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums;

public enum UserGroupKc {
    USER_GROUP("/users_mtfr"), MODERATOR_GROUP("/moderators_mtfr"), ADMIN_GROUP("/admins_mtfr");

    public final String name;

    UserGroupKc(String name) {
        this.name = name;
    }
}
