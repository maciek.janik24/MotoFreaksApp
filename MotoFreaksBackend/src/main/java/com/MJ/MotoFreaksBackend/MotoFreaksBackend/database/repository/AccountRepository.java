package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;


public interface AccountRepository extends MongoRepository<Account, String> {

    @Query("{'username': ?0}")
    Optional<Account> findByUserNameOptional(String userName);
}
