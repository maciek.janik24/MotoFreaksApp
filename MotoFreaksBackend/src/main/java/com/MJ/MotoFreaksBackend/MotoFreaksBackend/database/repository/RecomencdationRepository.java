package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Recommendation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecomencdationRepository extends MongoRepository<Recommendation, String> {
}
