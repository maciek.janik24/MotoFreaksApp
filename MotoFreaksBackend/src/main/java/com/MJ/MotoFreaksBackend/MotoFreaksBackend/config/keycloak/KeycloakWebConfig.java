package com.MJ.MotoFreaksBackend.MotoFreaksBackend.config.keycloak;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.RoleKc;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

@KeycloakConfiguration
@Import(KeycloakSpringBootConfigResolver.class)
public class KeycloakWebConfig extends KeycloakWebSecurityConfigurerAdapter {
    /**
     * Registers the KeycloakAuthenticationProvider with the authentication manager.
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());

        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    /**
     * Defines the session authentication strategy.
     */
    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(buildSessionRegistry());
    }

    @Bean
    protected SessionRegistry buildSessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()

                //Without Auth
                .antMatchers(
                        "/auth/kc/register",
                        "/webjars/**",
                        "/swagger-ui/**",
                        "/swagger-resources/**",
                        "/v2/api-docs").permitAll()

                //USER - GET
                .antMatchers(HttpMethod.GET,
                        "/auth/validation",
                        "/auth/roles",
                        "/cars/**",
                        "/challenge/**",
                        "/message/**",
                        "/posts/**",
                        "/sentence",
                        "/user/**",
                        "/account/**").hasRole(RoleKc.USER.name)

//                //USER- POST
                .antMatchers(HttpMethod.POST,
                        "/challenge/**/competitor/**",
                        "/message/read/**",
                        "/posts/**",
                        "/user/**",
                        "/account/**").hasRole(RoleKc.USER.name)

                // USER- PUT
                .antMatchers(HttpMethod.PUT,
                        "/challenge/**/competitor/**",
                        "/message/send/**",
                        "/posts/**",
                        "/user/**",
                        "/account/**").hasRole(RoleKc.USER.name)

//                //USER- DELETE
                .antMatchers(HttpMethod.DELETE,
                        "/posts/**",
                        "/user/**",
                        "/account/**").hasRole(RoleKc.USER.name)

//                //MODERATOR- PUT
                .antMatchers(HttpMethod.PUT,
                        "/cars/**",
                        "/challenge/**").hasRole(RoleKc.MODERATOR.name)

//                //MODERATOR- DELETE
                .antMatchers(HttpMethod.DELETE,
                        "/cars/**",
                        "/sentence/**").hasRole(RoleKc.MODERATOR.name)

//                //MODERATOR- POST
                .antMatchers(HttpMethod.POST,
                        "/auth/set/moderator/**",
                        "/cars/merge",
                        "/challenge/**",
                        "/sentence/merge").hasRole(RoleKc.MODERATOR.name)

//                //ADMIN -ALL
                .antMatchers("/**").hasRole(RoleKc.ADMIN.name)
                .anyRequest().authenticated();


        http
                .csrf()
                .disable();
    }
}