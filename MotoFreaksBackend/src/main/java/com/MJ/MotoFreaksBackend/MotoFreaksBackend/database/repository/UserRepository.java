package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;


@Deprecated
public interface UserRepository extends MongoRepository<User, String> {

    @Query("{'userName': ?0}")
    Optional<User> findByUserNameOptional(String userName);

}
