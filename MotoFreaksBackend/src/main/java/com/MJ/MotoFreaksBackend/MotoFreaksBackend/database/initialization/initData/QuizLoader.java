package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.initialization.initData;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Challenge;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.QuestionAnswer;

import java.util.*;

public class QuizLoader {

    public List<Challenge> quizToLoad;


    public QuizLoader(Date nowDate) {
        quizToLoad = new ArrayList<>();

        Challenge quiz = new Challenge();
        quiz.setCreatedDate(nowDate);
        quiz.setUpdatedDate(null);
        quiz.setCreatorId("sys");
        quiz.setGeneral(true);
        quiz.setCompetitorIdList(new HashMap<>());
        quiz.setName("Podstawowy quiz!");

        List<QuestionAnswer> questions = new ArrayList<>();

        QuestionAnswer first = new QuestionAnswer();
        first.setQuestion("Co oznacza skrót FWD?");
        first.setPoints(10);
        first.setCorrectAnswer("Napęd na przednią oś");
        first.setAnswers(Arrays.asList("Napęd na tylnią oś", "Napęd na przednią oś", "Napęd 4X4"));
        questions.add(first);

        QuestionAnswer second = new QuestionAnswer();
        second.setQuestion("Jak oznaczamy napęd 4X4 w marce Audi?");
        second.setPoints(15);
        second.setCorrectAnswer("Quattro");
        second.setAnswers(Arrays.asList("4 Matic","4 Motion", "Quattro", "4X4", "xDrive"));
        questions.add(second);

        QuestionAnswer third = new QuestionAnswer();
        third.setQuestion("Kultowa \"beczka\", to Mercedes o oznaczeniu: ");
        third.setPoints(15);
        third.setCorrectAnswer("W123");
        third.setAnswers(Arrays.asList("W124","W123", "W115", "W111"));
        questions.add(third);

        quiz.setQaList(questions);
        quizToLoad.add(quiz);

    }
}
