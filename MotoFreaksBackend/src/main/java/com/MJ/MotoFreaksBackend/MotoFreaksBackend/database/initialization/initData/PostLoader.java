package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.initialization.initData;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Post;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.CarDataModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.PostType;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PostLoader {

    public List<Post> postsToload;

    public PostLoader(Date nowDate){
        postsToload = new ArrayList<>();

        Post post1 = new Post();
        post1.setCreatedDate(nowDate);
        post1.setCreatorId("sys");
        post1.setTitle("Post powitalny!!!");
        post1.setType(PostType.INFO);
        post1.setBody("Cześć, miło cię widzieć na naszym forum w gronie miłośników motoryzacji. Zapraszamy do korzystania z dostępnych fukncjonalności: Posty, Quizy, Słownik, Chat. Jesteśmy w trakcie rozwiajania się. Pozdrawiamy, Zespół MTFR:)");
        post1.setComments(new ArrayList<>());
        Address address = new Address();
        address.setCity("Cracow");
        address.setCountry("PL");
        address.setStreet("Poznanska");
        address.setState("małopolska");
        post1.setLocation(address);
        post1.setCar(new CarDataModel(new ObjectId().toString(), nowDate, null, "My Car", "KK11111".toUpperCase(), "Audi"
                , "TT", "8N", 1999, "black", "1.8T", 270, 320));
        post1.setUserIdLikes(new ArrayList<>());
        postsToload.add(post1);
    }
}
