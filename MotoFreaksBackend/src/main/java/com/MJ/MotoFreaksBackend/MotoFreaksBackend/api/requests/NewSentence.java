package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import lombok.Data;

@Data
public class NewSentence {

    private String id;
    private String name;
    private String translation;
}
