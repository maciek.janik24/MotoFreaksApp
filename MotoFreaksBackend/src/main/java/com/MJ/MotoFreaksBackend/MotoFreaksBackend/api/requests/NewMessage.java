package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import lombok.Data;

@Data
public class NewMessage {
    private String content;
}
