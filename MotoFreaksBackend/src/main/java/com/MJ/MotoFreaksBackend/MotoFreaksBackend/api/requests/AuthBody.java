package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import lombok.Data;

@Data
@Deprecated
public class AuthBody {

    private String username;
    private String password;
}

