package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.CarDataModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Contact;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Message;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.Gender;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Document(collection = "Users")
@Data
@Deprecated
public class User {
    @Id
    private String id;
    private String userName;
    private String password;
    private String name;
    private String lastName;
    private Gender gender;
    private boolean enabled;
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date createdDate;
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date updatedDate;
    private List<Date> loginsHistory;
    private Set<UserRoles> userRoles;
    private List<CarDataModel> carsList;
    private Contact contact;
    private Address address;
    private Integer points;
    private List<String> friendsList;
    private Map<String, List<Message>> messages;


}
