package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import lombok.Data;

@Data
@Deprecated
public class RegisterBody {
    private String username;
    private String password;
    private String name;
    private String lastName;
    private String email;

}
