package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Message;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.UserBasicInfoModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageData {
    private String receiverId;
    private UserBasicInfoModel receiverData;
    private Date lastMessageDate;
    private Message lastMessage;
}
