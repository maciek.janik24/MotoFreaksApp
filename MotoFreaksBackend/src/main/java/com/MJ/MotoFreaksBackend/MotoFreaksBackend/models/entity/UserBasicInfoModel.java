package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserBasicInfoModel {
    private String name;
    private String lastName;
}
