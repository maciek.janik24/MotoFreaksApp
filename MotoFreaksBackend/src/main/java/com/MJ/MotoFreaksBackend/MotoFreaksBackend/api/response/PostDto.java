package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.CarDataModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.UserBasicInfoModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.PostState;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.PostType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Data
public class PostDto {

    private String id;
    private PostType type;
    private String title;
    private String body;
    private Date createdDate;
    private String creatorId;
    private PostState state;
    private List<String> userIdLikes;
    private Address location;
    private CarDataModel car;
    private List<CommentDto> comments;
    private UserBasicInfoModel creatorData;
}
