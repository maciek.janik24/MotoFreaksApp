package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.QuestionAnswer;
import lombok.Data;

import java.util.List;

@Data
public class NewChallengeModel {
    private String name;
    private boolean general;
    private String company;
    private String model;
    private String generation;
    private List<QuestionAnswer> qaList;
}
