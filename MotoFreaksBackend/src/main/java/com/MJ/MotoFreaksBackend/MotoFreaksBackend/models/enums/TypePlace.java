package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums;

public enum TypePlace {
    SHOP,
    ESHOP,
    MECHANIC,
    PARKING
}
