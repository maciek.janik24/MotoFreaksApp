package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.config.consts.AuthKcConsts;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Map;

@Service
public class KeycloakTokenService {

//    public String getUserId(){
//        KeycloakAuthenticationToken authentication = (KeycloakAuthenticationToken)
//                SecurityContextHolder.getContext().getAuthentication();
//        Principal principal = (Principal) authentication.getPrincipal();
//
//        return Objects.nonNull(principal.getName()) ? principal.getName() : "";
//        //throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
//        //todo
//    }


    public String getAccountId(){
        KeycloakAuthenticationToken authentication = (KeycloakAuthenticationToken)
                SecurityContextHolder.getContext().getAuthentication();

        Principal principal = (Principal) authentication.getPrincipal();
        String accountId="";

        if (principal instanceof KeycloakPrincipal) {
            KeycloakPrincipal kPrincipal = (KeycloakPrincipal) principal;
            AccessToken token = kPrincipal.getKeycloakSecurityContext().getToken();

            Map<String, Object> customClaims = token.getOtherClaims();

            if (customClaims.containsKey(AuthKcConsts.MTFR_ACCOUNT_KEY_NAME)) {
                accountId = String.valueOf(customClaims.get(AuthKcConsts.MTFR_ACCOUNT_KEY_NAME));
            }
        }

        return accountId;
    }
}
