package com.MJ.MotoFreaksBackend.MotoFreaksBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.MJ.MotoFreaksBackend.MotoFreaksBackend")
public class MotoFreaksBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MotoFreaksBackendApplication.class, args);
	}

}
