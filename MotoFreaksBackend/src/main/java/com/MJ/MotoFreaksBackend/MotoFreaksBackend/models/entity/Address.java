package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity;

import lombok.Data;

@Data
public class Address {

    private String country;
    private String city;
    private String street;
    private String state;
    }
