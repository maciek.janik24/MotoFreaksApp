package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.controller;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.AuthAccountCreateModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services.KeycloakApiService;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services.KeycloakService;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("auth/kc")
@CrossOrigin("*")
public class KeycloackController {

    private final KeycloakService service;
    private final KeycloakApiService keycloakApiService;

    public KeycloackController(KeycloakService service, KeycloakApiService keycloakApiService) {
        this.service = service;
        this.keycloakApiService = keycloakApiService;
    }

    @PutMapping(path = "/register")
    public Object addUser(@Valid @RequestBody AuthAccountCreateModel authAccountCreateModel){
        return keycloakApiService.addUser(authAccountCreateModel);
    }

    @GetMapping(path = "/{userName}")
    public List<UserRepresentation> getUser(@PathVariable("userName") String userName){
        return service.getUser(userName);
    }

//    @PutMapping(path = "/update/{userId}")
//    public String updateUser(@PathVariable("userId") String userId,   @RequestBody AccountCreateModel accountCreateModel){
//        service.updateUser(userId, accountCreateModel);
//        return "User Details Updated Successfully.";
//    }



//    @GetMapping(path = "/verification-link/{userId}")
//    public String sendVerificationLink(@PathVariable("userId") String userId){
//        service.sendVerificationLink(userId);
//        return "Verification Link Send to Registered E-mail Id.";
//    }

    @GetMapping(path = "/reset-password/{userId}")
    public String sendResetPassword(@PathVariable("userId") String userId){
        service.sendResetPassword(userId);
        return "Reset Password Link Send Successfully to Registered E-mail Id.";
    }
}