package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.initialization.initData;


import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.CarCompany;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class CarsLoader {

    public List<CarCompany> carsToLoad;

    public CarsLoader(Date nowDate) {
        carsToLoad = new ArrayList<>();


        //MINI
        CarCompany mini = new CarCompany();
        mini.setCreatedDate(nowDate);
        mini.setCreatorId("sys");
        mini.setCompany("MINI");
        mini.setModelList(Stream.of(
                        new AbstractMap.SimpleEntry<>("Cooper", Arrays.asList("R50", "R53", "R56", "F56")),
                        new AbstractMap.SimpleEntry<>("Countryman", Arrays.asList("F60", "R60")),
                        new AbstractMap.SimpleEntry<>("Clubman", Arrays.asList("F54", "R55")),
                        new AbstractMap.SimpleEntry<>("Cabrio", Arrays.asList("F57", "R57","R52"))
                ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));


        //BMW
        CarCompany bmw = new CarCompany();
        bmw.setCreatedDate(nowDate);
        bmw.setCreatorId("sys");
        bmw.setCompany("BMW");
        bmw.setModelList(Stream.of(
                        new AbstractMap.SimpleEntry<>("Seria 1", Arrays.asList("E81/E86", "F20-F21", "F40")),
                        new AbstractMap.SimpleEntry<>("Seria 2", Arrays.asList("F22-F23-F45-F46", "G42-U06")),
                        new AbstractMap.SimpleEntry<>("Seria 3", Arrays.asList("E21", "E30", "E36", "E46", "E90-91-92-93", "F30-F31-F34", "G20-G21")),
                        new AbstractMap.SimpleEntry<>("Seria 4", Arrays.asList("F32-F33-F36", "G22-G23-G26")),
                        new AbstractMap.SimpleEntry<>("Seria 5", Arrays.asList("E12", "E28", "E34", "E39", "E60", "F10-F11", "G30-G31")),
                        new AbstractMap.SimpleEntry<>("Seria 6", Arrays.asList("E24", "E63", "F06-F12-F13", "G32")),
                        new AbstractMap.SimpleEntry<>("Seria 7", Arrays.asList("E23", "E32", "E38", "E65", "F01", "G11-F12", "G70")),
                        new AbstractMap.SimpleEntry<>("X1", Arrays.asList("E84", "F48", "U11")),
                        new AbstractMap.SimpleEntry<>("X2", Collections.singletonList("Crossover")),
                        new AbstractMap.SimpleEntry<>("X3", Arrays.asList("E83", "F25", "G01")),
                        new AbstractMap.SimpleEntry<>("X4", Arrays.asList("G01", "G02")),
                        new AbstractMap.SimpleEntry<>("X5", Arrays.asList("E53", "E70", "F15", "G05")),
                        new AbstractMap.SimpleEntry<>("X6", Arrays.asList("E71", "F16", "G06")),
                        new AbstractMap.SimpleEntry<>("X7", Collections.singletonList("SUV")),
                        new AbstractMap.SimpleEntry<>("Z4", Arrays.asList("E85", "E89", "G29"))
                ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));


        //AUDI
        CarCompany audi = new CarCompany();
        audi.setCreatedDate(nowDate);
        audi.setCreatorId("sys");
        audi.setCompany("Audi");
        audi.setModelList(Stream.of(
                        new AbstractMap.SimpleEntry<>("TT", Arrays.asList("8N", "8J", "8S")),
                        new AbstractMap.SimpleEntry<>("R8", Arrays.asList("I", "II")),
                        new AbstractMap.SimpleEntry<>("A1", Arrays.asList("I", "II")),
                        new AbstractMap.SimpleEntry<>("A3", Arrays.asList("8L", "8P", "8V", "8Y")),
                        new AbstractMap.SimpleEntry<>("A4", Arrays.asList("B5", "B6", "B7", "B8", "B9")),
                        new AbstractMap.SimpleEntry<>("A5", Arrays.asList("I", "II")),
                        new AbstractMap.SimpleEntry<>("A6", Arrays.asList("C4", "C5", "C6", "C7", "C8")),
                        new AbstractMap.SimpleEntry<>("A7", Arrays.asList("I", "II")),
                        new AbstractMap.SimpleEntry<>("A8", Arrays.asList("D2", "D3", "D4", "D5")),
                        new AbstractMap.SimpleEntry<>("Q2", Arrays.asList("SUV", "SUV-Facelifting")),
                        new AbstractMap.SimpleEntry<>("Q3", Arrays.asList("I", "II")),
                        new AbstractMap.SimpleEntry<>("Q4", Arrays.asList("E-TRON", "E-TRON SPORTBACK")),
                        new AbstractMap.SimpleEntry<>("Q5", Arrays.asList("I", "II")),
                        new AbstractMap.SimpleEntry<>("Q7", Arrays.asList("I", "II")),
                        new AbstractMap.SimpleEntry<>("Q8", Arrays.asList("SUV", "Q8-E"))
                ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));


        //MITSUBISHI
        CarCompany mitsubishi = new CarCompany();
        mitsubishi.setCreatedDate(nowDate);
        mitsubishi.setCreatorId("sys");
        mitsubishi.setCompany("Mitsubishi");
        mitsubishi.setModelList(Stream.of(
                        new AbstractMap.SimpleEntry<>("L200", Arrays.asList("I", "II", "III", "IV", "V", "VI")),
                        new AbstractMap.SimpleEntry<>("Outlander", Arrays.asList("I", "II", "III")),
                        new AbstractMap.SimpleEntry<>("Carisma", Arrays.asList("Sedan", "Hatchback")),
                        new AbstractMap.SimpleEntry<>("Lancer", Arrays.asList("I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X")),
                        new AbstractMap.SimpleEntry<>("Pajero", Arrays.asList("I", "II", "Pinin", "III", "IV"))
                ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

        //VOLKSWAGEN
        CarCompany volkswagen = new CarCompany();
        volkswagen.setCreatedDate(nowDate);
        volkswagen.setCreatorId("sys");
        volkswagen.setCompany("Volkswagen");
        volkswagen.setModelList(Stream.of(
                        new AbstractMap.SimpleEntry<>("Arteon", Arrays.asList("Fastback", "Shooting brake")),
                        new AbstractMap.SimpleEntry<>("Caddy", Arrays.asList("I", "II", "III", "IV", "V")),
                        new AbstractMap.SimpleEntry<>("Caravelle", Arrays.asList("T1", "T2", "T3", "T4", "T5", "T6")),
                        new AbstractMap.SimpleEntry<>("Crafter", Collections.singletonList("I")),
                        new AbstractMap.SimpleEntry<>("Golf", Arrays.asList("I", "II", "III", "IV", "V", "VI","VII","VIII")),
                        new AbstractMap.SimpleEntry<>("Polo", Arrays.asList("I", "II", "III", "IV", "V", "VI")),
                        new AbstractMap.SimpleEntry<>("Sharan", Arrays.asList("I", "II")),
                        new AbstractMap.SimpleEntry<>("Passat", Arrays.asList("B1", "B2", "B3","B4","B5","B6","B7","B8")),
                        new AbstractMap.SimpleEntry<>("Tiguan", Arrays.asList("I","II")),
                        new AbstractMap.SimpleEntry<>("Tuareg", Arrays.asList("I","II","III")),
                        new AbstractMap.SimpleEntry<>("Touran", Arrays.asList("I","II","III"))
                ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));


        carsToLoad.add(audi);
        carsToLoad.add(bmw);
        carsToLoad.add(mini);
        carsToLoad.add(mitsubishi);
        carsToLoad.add(volkswagen);
    }

}
