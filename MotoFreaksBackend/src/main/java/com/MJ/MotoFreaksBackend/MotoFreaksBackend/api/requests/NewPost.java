package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.CarDataModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.PostType;
import lombok.Data;

@Data
public class NewPost {

    private PostType type;
    private String title;
    private String body;
    private Address location;
    private CarDataModel car;
}
