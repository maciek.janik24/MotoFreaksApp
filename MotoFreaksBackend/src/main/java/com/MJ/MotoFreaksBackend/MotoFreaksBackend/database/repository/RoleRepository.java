package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.UserRoles;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.Role;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
@Deprecated
public interface RoleRepository extends MongoRepository<UserRoles, String> {
    Optional<UserRoles> findByRole(Role role);

}
