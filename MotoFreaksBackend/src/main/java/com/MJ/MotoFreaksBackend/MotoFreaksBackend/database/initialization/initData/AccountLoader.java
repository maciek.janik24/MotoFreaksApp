package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.initialization.initData;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Account;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.CarDataModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Contact;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.Gender;
import org.bson.types.ObjectId;

import java.util.*;

public class AccountLoader {

    public List<Account> accountToLoad;


    public AccountLoader(Date now) {
        accountToLoad = new ArrayList<>();


        Account initAdminAccount = new Account();
        initAdminAccount.setId("my-admin-mtfr-init-account");
        initAdminAccount.setUsername("admin-mtfr");
        initAdminAccount.setCreatedDate(now);
        initAdminAccount.setEnabled(true);
        initAdminAccount.setName("Admin");
        initAdminAccount.setLastName("MTFR");
        initAdminAccount.setGender(Gender.MALE);
        initAdminAccount.setCreatedDate(now);
        initAdminAccount.setPoints(0);
        initAdminAccount.setFriendsList(new ArrayList<>());
        initAdminAccount.setMessages(new HashMap<>());
        initAdminAccount.setLoginsHistory(new ArrayList<>());

        initAdminAccount.setContact(new Contact("admin@com.com"));
        Address address = new Address();
        address.setCity("Cracow");
        address.setCountry("PL");
        address.setStreet("Poznanska");
        address.setState("małopolska");
        initAdminAccount.setAddress(address);
        initAdminAccount.setCarsList(Collections.singletonList(new CarDataModel(new ObjectId().toString(), now, null, "My Car", "KK11111".toUpperCase(), "Audi"
                , "TT", "8N", 1999, "black", "1.8T", 270, 320)));


        accountToLoad.add(initAdminAccount);
    }


}
