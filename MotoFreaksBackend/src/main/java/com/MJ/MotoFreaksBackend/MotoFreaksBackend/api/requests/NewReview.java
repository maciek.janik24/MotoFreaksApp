package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import lombok.Data;

@Data
public class NewReview {

    private String body;
    private Integer points;

}
