package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums;

public enum ChallengeStateForUser {
    ALL,
    FILLED,
    UNFILLED
}
