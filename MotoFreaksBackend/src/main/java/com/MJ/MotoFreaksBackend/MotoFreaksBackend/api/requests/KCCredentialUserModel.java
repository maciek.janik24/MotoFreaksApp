package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KCCredentialUserModel {
    private String type;
    private String value;
    private String temporary;
}
