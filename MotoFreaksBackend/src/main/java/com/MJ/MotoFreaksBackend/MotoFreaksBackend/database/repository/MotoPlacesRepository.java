package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.MotoPlaces;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MotoPlacesRepository extends MongoRepository<MotoPlaces, String> {
}
