package com.MJ.MotoFreaksBackend.MotoFreaksBackend.config.consts;

public final class AuthKcConsts {
    public final static String HEADER_NAME = "Authorization";
    public final static String TOKEN_PREFIX = "Bearer ";
    public final static String MTFR_ACCOUNT_KEY_NAME = "mtfrAccountId";
    public final static String OTP_ACTION = "CONFIGURE_TOTP";
    public final static String CREDENTIAL_TYPE = "password";


}
