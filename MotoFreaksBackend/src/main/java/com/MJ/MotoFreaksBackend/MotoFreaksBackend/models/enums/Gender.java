package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums;

public enum Gender {
    MALE, FEMALE
}
