package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums;

public enum TypeRecomendation {
    SHOP,
    ESHOP,
    MECHANIC,
    PARKING
}
