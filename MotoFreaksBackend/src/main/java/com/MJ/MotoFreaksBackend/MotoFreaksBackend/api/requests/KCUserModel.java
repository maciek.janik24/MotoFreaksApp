package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class KCUserModel {

    private String firstName;
    private String lastName;
    private String email;
    private String enabled;
    private String username;
    private Map<String, List<String>> attributes;
    private List<KCCredentialUserModel> credentials;
    private List<String> groups;
    private List<String> requiredActions;

}
