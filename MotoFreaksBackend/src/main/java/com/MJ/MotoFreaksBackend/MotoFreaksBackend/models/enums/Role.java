package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums;

@Deprecated
public enum Role {
    MODERATOR, USER, ADMIN
}
