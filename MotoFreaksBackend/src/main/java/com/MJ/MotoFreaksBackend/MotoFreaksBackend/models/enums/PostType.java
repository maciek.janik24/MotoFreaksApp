package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums;

public enum PostType {
    ADVICE, INFO, BORROW, ROAD_HELP
}
