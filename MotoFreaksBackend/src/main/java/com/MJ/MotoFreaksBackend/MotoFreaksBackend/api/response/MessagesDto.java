package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Message;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.UserBasicInfoModel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class MessagesDto {

    private List<Message> messages;
    private UserBasicInfoModel receiverData;

}