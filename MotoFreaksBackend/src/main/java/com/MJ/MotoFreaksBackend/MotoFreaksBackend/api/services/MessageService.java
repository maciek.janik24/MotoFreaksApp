package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.NewMessage;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response.MessageData;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response.MessagesDto;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Account;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository.AccountRepository;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@Service
@Slf4j
public class MessageService {
    private final AccountService accountService;
    private final AccountRepository accountRepository;

    public MessageService(AccountService accountService, AccountRepository accountRepository) {

        this.accountService = accountService;
        this.accountRepository = accountRepository;
    }

    public Object sendMessage(String receiverId, NewMessage messageContent) {
        Map<Object, Object> model = new HashMap<>();
        Message message = new Message(messageContent.getContent(), new Date());
        Account senderAccount = accountService.getAccountByToken();
        Account receiverAccount = accountService.getAccountById(receiverId);
        this.accountRepository.save(addMessage(senderAccount, receiverAccount.getId(), message, false));
        this.accountRepository.save(addMessage(receiverAccount, senderAccount.getId(), message, true));
        log.info("Message sent to " + receiverId + " from " + senderAccount.getId());
        model.put("message", "Message sent to " + receiverId + " from " + senderAccount.getId());
        return getMessages(receiverId);
    }

    private Account addMessage(Account user, String secondId, Message message, boolean isReceived) {
        message.setReceived(isReceived);
        message.setRead(!isReceived);
        message.setReadDate(!isReceived ? new Date() : null);
        if (user.getMessages().get(secondId) == null) {
            List<Message> messageList = new ArrayList<>();
            messageList.add(message);
            user.getMessages().put(secondId, messageList);
        } else {
            user.getMessages().get(secondId).add(message);
        }
        return user;
    }

    public Object readMessage(String receiverId) {
        Map<Object, Object> model = new HashMap<>();
        Account currentAccount = accountService.getAccountByToken();
        if (currentAccount.getMessages().keySet().stream().anyMatch(key -> key.equals(receiverId))) {
            currentAccount.getMessages().get(receiverId).stream().filter(message -> !message.isRead()).forEach(unreadMessage -> {
                unreadMessage.setRead(true);
                unreadMessage.setReadDate(new Date());
            });
        }
        this.accountRepository.save(currentAccount);
        model.put("message", "Messages to " + currentAccount.getId() + " user from " + receiverId + " user is read now.");
        return ok(model);
    }

    public Object getUnreadMessage() {
        AtomicReference<Long> count = new AtomicReference<>((long) 0);
        Account currentAccount = accountService.getAccountByToken();
        currentAccount.getMessages().values().forEach(messages -> {
            count.updateAndGet(v -> v + messages.stream().filter(message -> !message.isRead()).count());
        });
        return ok(count);
    }

    public Object getChatsInfo() {
        Account currentAccount = accountService.getAccountByToken();
        List<MessageData> allChats = new ArrayList<>();
        currentAccount.getMessages().keySet().forEach(key -> {
            Message tempMessage = currentAccount.getMessages().get(key).get(currentAccount.getMessages().get(key).size() - 1);
            allChats.add(new MessageData(key, this.accountService.getCreatorData(key), tempMessage.getCreatedDate(), tempMessage));
        });

        return ok(allChats.stream().sorted(Comparator.comparing(MessageData::getLastMessageDate).reversed()).collect(Collectors.toList()));
    }

    public Object getMessages(String receiverId) {
        Account currentAccount = accountService.getAccountByToken();
        List<Message> allMessages = new ArrayList<>();
        if (currentAccount.getMessages().keySet().stream().anyMatch(key -> key.equals(receiverId))) {
            allMessages = currentAccount.getMessages().get(receiverId);
        }
        MessagesDto response = new MessagesDto(allMessages, this.accountService.getCreatorData(receiverId));
        readMessage(receiverId);
        return ok(response);
    }
}
