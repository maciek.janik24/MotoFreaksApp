package com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity;

import lombok.Data;

@Data
public class PointsReviews {

    private String reviewerId;
    private Integer points;
}
