package com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Post;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.PostType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PostsRepository extends MongoRepository<Post, String> {

    @Query("{'creatorId': ?0}")
    Optional<List<Post>> findByCreatorIdOptional(String creatorId);

    @Query("{'type': ?0}")
    Optional<List<Post>> findByTypeOptional(PostType type);
}
