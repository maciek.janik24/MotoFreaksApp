package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.AuthAccountCreateModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.MergeAccount;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.NewCar;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response.AccountDto;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response.MyAccountDto;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.response.MyFriendDto;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.Account;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository.AccountRepository;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.CarDataModel;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Contact;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.UserBasicInfoModel;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@Service
@Slf4j
public class AccountService {
    private final AccountRepository accountRepository;
    private final KeycloakTokenService keycloakTokenService;

    public AccountService(AccountRepository accountRepository, KeycloakTokenService keycloakTokenService) {
        this.accountRepository = accountRepository;
        this.keycloakTokenService = keycloakTokenService;
    }

    public Account getAccountByToken() {
        Optional<Account> optionalAccount = accountRepository.findById(keycloakTokenService.getAccountId());
        return optionalAccount.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found"));
    }

    public Account getAccountByUserName(String username) {
        Optional<Account> optionalAccount = accountRepository.findByUserNameOptional(username);
        return optionalAccount.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found"));
    }

    public Account addNewAccount(AuthAccountCreateModel authAccountCreateModel) {
        Date now = new Date();
        Account newAccount = new Account();
        newAccount.setCreatedDate(now);
        newAccount.setEnabled(true);
        newAccount.setUsername(authAccountCreateModel.getUsername());
        newAccount.setName(authAccountCreateModel.getFirstname());
        newAccount.setLastName(authAccountCreateModel.getLastName());
        newAccount.setCreatedDate(now);
        newAccount.setPoints(0);
        newAccount.setFriendsList(new ArrayList<>());
        newAccount.setMessages(new HashMap<>());
        newAccount.setLoginsHistory(new ArrayList<>());
        newAccount.setContact(new Contact(authAccountCreateModel.getEmail()));
        newAccount.setCarsList(new ArrayList<>());
        newAccount.setAddress(new Address());

        accountRepository.save(newAccount);

        return accountRepository.findByUserNameOptional(authAccountCreateModel.getUsername()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found"));
    }

    public void deleteAccount(String id) {
        accountRepository.deleteById(id);
    }

    public Account getAccountById(String id) {
        Optional<Account> optionalAccountFriend = accountRepository.findById(id);
        return optionalAccountFriend.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Account not found"));
    }


    public Object addCar(NewCar car) {
        Map<Object, Object> model = new HashMap<>();
        Account currentAccount = getAccountByToken();
        if (Objects.isNull(currentAccount.getCarsList())) {
            currentAccount.setCarsList(new ArrayList<>());
        }
        currentAccount.getCarsList().add(new CarDataModel(new ObjectId().toString(), new Date(), null, car.getName(), car.getRegistration().toUpperCase(), car.getCompany()
                , car.getModel(), car.getGeneration(), car.getYear(), car.getColor(), car.getEngine(), car.getHorsepower(), car.getTorque()));
        currentAccount.setUpdatedDate(new Date());
        accountRepository.save(currentAccount);
        model.put("message", "Car added successful.");
        log.info("Car " + car.getName() + " added to " + currentAccount.getId() + " Account.");
        return ok(model);
    }

    public Object removeCar(String id) {
        Map<Object, Object> model = new HashMap<>();
        Account currentAccount = getAccountByToken();
        currentAccount.getCarsList().removeIf(carDataModel -> carDataModel.getId().equals(id));
        accountRepository.save(currentAccount);
        model.put("message", "Car removed successful.");
        log.info("Car " + id + " removed from " + currentAccount.getId() + " Account.");
        return ok(model);
    }

    public Object mergeCar(NewCar car, String carId) {
        Map<Object, Object> model = new HashMap<>();
        Account currentAccount = getAccountByToken();
        currentAccount.getCarsList().stream().filter(selectedCar -> selectedCar.getId().equals(carId)).forEach(carToEdit -> {
            carToEdit.setUpdatedDate(new Date());
            carToEdit.setName(car.getName());
            carToEdit.setRegistration(car.getRegistration());
            carToEdit.setCompany(car.getCompany());
            carToEdit.setModel(car.getModel());
            carToEdit.setGeneration(car.getGeneration());
            carToEdit.setYear(car.getYear());
            carToEdit.setColor(car.getColor());
            carToEdit.setEngine(car.getEngine());
            carToEdit.setHorsepower(car.getHorsepower());
            carToEdit.setTorque(car.getTorque());
        });
        accountRepository.save(currentAccount);
        model.put("message", "Car merged successful.");
        log.info("Car " + car.getName() + " merged for " + currentAccount.getId() + " Account.");
        return ok(model);
    }

    public Object mergeAccount(MergeAccount mergeAccount) {
        Map<Object, Object> model = new HashMap<>();
        Account currentAccount = getAccountByToken();
        currentAccount.setName(mergeAccount.getName());
        currentAccount.setLastName(mergeAccount.getLastName());
        currentAccount.setEnabled(mergeAccount.isEnabled());
        currentAccount.setGender(mergeAccount.getGender());
        currentAccount.setUpdatedDate(new Date());
        //TODO change password
        //TODO KC Account change
//        if (!(mergeAccount.getPassword().isEmpty())) {
//            currentAccount.setPassword(bCryptPasswordEncoder.encode(mergeAccount.getPassword()));
//        }
        accountRepository.save(currentAccount);
        model.put("message", "Account " + currentAccount.getId() + " was updated");
        log.info("Account " + currentAccount.getId() + " was updated");
        return ok(model);
    }


    public Object mergeAddress(Address address) {
        Map<Object, Object> model = new HashMap<>();
        Account currentAccount = getAccountByToken();
        currentAccount.setAddress(address);
        currentAccount.setUpdatedDate(new Date());
        accountRepository.save(currentAccount);
        model.put("message", "Address was merged for " + currentAccount.getId() + " Account.");
        log.info("Address was merged for " + currentAccount.getId() + " Account.");
        return ok(model);
    }

    public Object mergeContact(Contact contact) {
        Map<Object, Object> model = new HashMap<>();
        Account currentAccount = getAccountByToken();
        currentAccount.setContact(contact);
        currentAccount.setUpdatedDate(new Date());
        accountRepository.save(currentAccount);
        model.put("message", "Contacts was merged for " + currentAccount.getId() + " Account.");
        log.info("Contacts was merged for " + currentAccount.getId() + " Account.");
        return ok(model);
    }

    public Object addFriend(String friendId) {
        Map<Object, Object> model = new HashMap<>();
        Account currentAccount = getAccountByToken();
        Account AccountFriend = getAccountById(friendId);
        if (isYourFriend(currentAccount, friendId) || currentAccount.getId().equals(friendId)) {
            model.put("message", "Cannot add " + AccountFriend.getId() + " to friends " + currentAccount.getId() + " Account.");
            log.warn("Cannot add " + AccountFriend.getId() + " to friends " + currentAccount.getId() + " Account.");
            return new ResponseEntity<>(model, HttpStatus.BAD_REQUEST);
        } else {
            currentAccount.getFriendsList().add(AccountFriend.getId());
            currentAccount.setUpdatedDate(new Date());
        }
        accountRepository.save(currentAccount);
        model.put("message", "Friend " + AccountFriend.getId() + " added to " + currentAccount.getId() + " Account.");
        log.info("Friend " + AccountFriend.getId() + " added to " + currentAccount.getId() + " Account.");
        return ok(model);
    }

    public Object getFriends() {
        Account currentAccount = getAccountByToken();
        List<MyFriendDto> myFriendList = new ArrayList<>();
        accountRepository.findAll().forEach(Account -> {
            currentAccount.getFriendsList().forEach(friendId -> {
                if (Account.getId().equals(friendId))
                    myFriendList.add(new MyFriendDto(Account.getId(), Account.getName(), Account.getLastName(), Account.getGender()));
            });
        });
        return ok(myFriendList);
    }

//    public Object getProfile(String id) {
//        Account currentAccount = getAccountByToken();
//        Account AccountToShow = getAccountById(id);
//        List<Role> roles = new ArrayList<>();
//        AccountToShow.getAccountRoles().forEach(AccountRoles -> roles.add(AccountRoles.getRole()));
//        AccountDto profile = new AccountDto(AccountToShow.getId(), AccountToShow.getName(), AccountToShow.getLastName(), AccountToShow.getGender(), AccountToShow.isEnabled()
//                , AccountToShow.getCarsList(), AccountToShow.getContact(), AccountToShow.getAddress(),
//                AccountToShow.getPoints(), AccountToShow.getFriendsList(), roles, isYourFriend(currentAccount, AccountToShow.getAccountName()));
//        return ok(profile);
//    }

    public Object getMyProfile() {
        Account currentAccount = getAccountByToken();
        MyAccountDto myProfile
                = new MyAccountDto(currentAccount.getId(), currentAccount.getUsername(), currentAccount.getName(), currentAccount.getLastName(), currentAccount.getGender(), currentAccount.isEnabled(),
                currentAccount.getCreatedDate(), currentAccount.getUpdatedDate(), currentAccount.getLoginsHistory(), currentAccount.getCarsList(),
                currentAccount.getContact(), currentAccount.getAddress(), currentAccount.getPoints());

        return ok(myProfile);
    }

    public Object addPoints(int points) {
        Map<Object, Object> model = new HashMap<>();
        Account currentAccount = getAccountByToken();
        currentAccount.setPoints(currentAccount.getPoints() + points);
        currentAccount.setUpdatedDate(new Date());
        accountRepository.save(currentAccount);
        model.put("message", points + " points added to " + currentAccount.getId() + " Account.");
        log.info(points + " points added to " + currentAccount.getId() + " Account.");
        return ok(model);
    }

    private boolean isYourFriend(Account currentAccount, String id) {
        if (currentAccount.getId().equals(id)) {
            return true;
        }
        String commonFriends = currentAccount.getFriendsList().stream().filter(id::equals).findAny().orElse("");
        return !commonFriends.isEmpty();
    }

    public Object getAllAccounts() {
        Account currentAccount = getAccountByToken();
        List<AccountDto> allAccounts = accountRepository.findAll().stream().map(account -> new AccountDto(
                account.getId(), account.getName(), account.getLastName(),
                account.getGender(), account.isEnabled(), account.getCarsList(),
                account.getContact(), account.getAddress(), account.getPoints(),
                account.getFriendsList(), isYourFriend(currentAccount, account.getId())
        )).collect(Collectors.toList());

        return new ResponseEntity<>(allAccounts, HttpStatus.OK);
    }

    public Object checkUser() {
        Account currentAccount = getAccountByToken();
        return ok(checkValidateUser(currentAccount));
    }

    private boolean checkValidateUser(Account currentAccount) {
        return currentAccount.getAddress() != null && currentAccount.getGender() != null && !currentAccount.getCarsList().isEmpty();
    }

    public UserBasicInfoModel getCreatorData(String id) {
        if (id.equals("sys")) {
            return new UserBasicInfoModel("System", "");
        }
        Account creatorAccount = getAccountById(id);
        return new UserBasicInfoModel(creatorAccount.getName(), creatorAccount.getLastName());
    }

}
