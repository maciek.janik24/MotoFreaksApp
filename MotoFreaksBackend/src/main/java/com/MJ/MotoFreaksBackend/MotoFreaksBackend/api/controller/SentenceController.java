package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.controller;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.NewSentence;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services.SentenceService;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.config.consts.AuthKcConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/sentence")
@CrossOrigin("*")
public class SentenceController {

    private final SentenceService sentenceService;

    @Autowired
    public SentenceController(SentenceService sentenceService) {
        this.sentenceService = sentenceService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET, produces = "application/json")
    public Object getAllSentence(){
        return sentenceService.getAllSorted();
    }

    @RequestMapping(path = "/merge", method = RequestMethod.POST, produces = "application/json")
    public Object mergeSentence(HttpServletRequest req, @RequestBody NewSentence newSentence){
        String token = req.getHeader(AuthKcConsts.HEADER_NAME).replace(AuthKcConsts.TOKEN_PREFIX, "");
        return sentenceService.merge(token,newSentence);
    }
    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public Object deleteSentence(@PathVariable String id){
        return sentenceService.delete(id);
    }
}
