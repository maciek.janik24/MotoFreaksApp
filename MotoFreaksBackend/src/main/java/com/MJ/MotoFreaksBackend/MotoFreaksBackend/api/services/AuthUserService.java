package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.AuthBody;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.RegisterBody;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.User;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.collections.UserRoles;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.database.repository.UserRepository;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Contact;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

import static org.springframework.http.ResponseEntity.ok;

@Service
@Slf4j
@Deprecated
public class AuthUserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private KeycloakTokenService keycloakTokenService;

    public Object loginUser(AuthBody data) {
        //TODO KC change

//        String username = data.getUsername();
////        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
//        User currentUser = userService.getUserByUserName(username);
////        String token = jwtTokenProvider.createToken(username, currentUser.getUserRoles());
//        currentUser.getLoginsHistory().add(new Date());
//        userRepository.save(currentUser);
//        List<Role> roles = new ArrayList<>();
//        currentUser.getUserRoles().forEach(userRoles -> roles.add(userRoles.getRole()));
//
//        LoginModelDto loginSuccess = new LoginModelDto(username, checkValidateUser(currentUser), AuthKcConsts.TOKEN_PREFIX + "EMPTY TOKEN", roles);
//        log.info("User " + currentUser.getId() + " was logged correctly.");


        return ok(keycloakTokenService.getAccountId());
    }

    private boolean checkValidateUser(User currentUser) {
        boolean validation = currentUser.getAddress() != null && currentUser.getGender() != null && !currentUser.getCarsList().isEmpty();
        return validation;
    }

    public Object registerUser(RegisterBody data, Role role) {
        Map<Object, Object> model = new HashMap<>();
        try {
            userService.getUserByUserName(data.getUsername());
            log.warn("Cannot register user: " + data.getUsername() + ". User is already exists");
            return new ResponseEntity<Object>("User  '" + data.getUsername() + "' is already exists!", HttpStatus.NOT_FOUND);
        } catch (ResponseStatusException e) {
            saveNewUser(data, role);
            model.put("message", "User registered successfully");
            log.info("User " + data.getUsername() + " was register correctly.");
            return ok(model);
        }
    }

    public void saveNewUser(RegisterBody user, Role role) {
        User newUser = new User();
        newUser.setUserName(user.getUsername());
        ///TODO change register handling
            //TODO KC User change
//        newUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        newUser.setEnabled(true);
        newUser.setName(user.getName());
        newUser.setLastName(user.getLastName());
        newUser.setCreatedDate(new Date());
        newUser.setPoints(0);
        newUser.setFriendsList(new ArrayList<>());
        newUser.setMessages(new HashMap<>());
        newUser.setLoginsHistory(new ArrayList<>());
        newUser.setCarsList(new ArrayList<>());
        UserRoles userUserRoles = roleService.getRoleByName(role);
        newUser.setUserRoles(new HashSet<>(Collections.singletonList(userUserRoles)));
        newUser.setContact(new Contact(user.getEmail()));
        newUser.setAddress(new Address());
        userRepository.save(newUser);
    }

    public Object addRole(String id, Role role) {
        Map<Object, Object> model = new HashMap<>();
        User userExists = userService.getUserById(id);
        model.put("ID:", id);
        if (role == Role.ADMIN) {
            userExists.getUserRoles().add(this.roleService.getRoleByName(Role.MODERATOR));
            model.put("newRole", Role.MODERATOR.toString());
            log.info("Added " + Role.MODERATOR + " role to " + id + " user.");
        }
        userExists.getUserRoles().add(this.roleService.getRoleByName(role));
        model.put("newRole", role);
        this.userRepository.save(userExists);
        log.info("Added " + role + " role to " + id + " user.");
        return ok(model);
    }

    public Object getRoles(String token) {
        User currentUser = userService.getUserByToken(token);
        List<Role> roles = new ArrayList<>();
        currentUser.getUserRoles().forEach(userRoles -> roles.add(userRoles.getRole()));
        return ok(roles);
    }

    public Object checkUser(String token) {
        User currentUser = userService.getUserByToken(token);
        return ok(checkValidateUser(currentUser));
    }
}
