package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Review;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.enums.TypePlace;
import lombok.Data;

@Data
public class NewMotoPlace {

    private TypePlace type;
    private String name;
    private Address address;
    private String webPageUrl;
    private String creatorId;
    private Review reviewList;
}
