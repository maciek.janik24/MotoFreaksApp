package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.controller;

import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.MergeAccount;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.NewCar;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services.AccountService;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Address;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.models.entity.Contact;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
@CrossOrigin("*")
@Slf4j
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET, produces = "application/json")
    public Object getAll() {
        return accountService.getAllAccounts();
    }
    @RequestMapping(path = "/validation", method = RequestMethod.GET, produces = "application/json")
    public Object checkValidationUser() {
        return accountService.checkUser();
    }
    @RequestMapping(path = "/merge", method = RequestMethod.POST, produces = "application/json")
    public Object mergeAccount(@RequestBody MergeAccount Account) {
        return accountService.mergeAccount(Account);
    }

    @RequestMapping(path = "/address", method = RequestMethod.POST, produces = "application/json")
    public Object mergeAddress(@RequestBody Address address) {
        return accountService.mergeAddress(address);
    }

    @RequestMapping(path = "/contact", method = RequestMethod.POST, produces = "application/json")
    public Object mergeContact(@RequestBody Contact contact) {
        return accountService.mergeContact(contact);
    }

    @RequestMapping(path = "/car", method = RequestMethod.PUT, produces = "application/json")
    public Object addCar(@RequestBody NewCar car) {
        return accountService.addCar(car);
    }

    @RequestMapping(path = "/car/{carId}", method = RequestMethod.DELETE, produces = "application/json")
    public Object deleteCar(@PathVariable String carId) {
        return accountService.removeCar(carId);
    }

    @RequestMapping(path = "/car/{carId}", method = RequestMethod.POST, produces = "application/json")
    public Object mergeCar(@RequestBody NewCar car, @PathVariable String carId) {
        return accountService.mergeCar(car, carId);
    }

    @RequestMapping(path = "/friend/{friendId}", method = RequestMethod.PUT, produces = "application/json")
    public Object addFriend(@PathVariable String friendId) {
        return accountService.addFriend(friendId);
    }

    @RequestMapping(path = "/friends", method = RequestMethod.GET, produces = "application/json")
    public Object getFriends() {
        return accountService.getFriends();
    }

    ///TODO Add endpoint 
//    @RequestMapping(path = "/profile/{id}", method = RequestMethod.GET, produces = "application/json")
//    public Object showProfile(@PathVariable String id, HttpServletRequest req) {
//        String token = req.getHeader(AuthKcConsts.HEADER_NAME).replace(AuthKcConsts.TOKEN_PREFIX, "");
//        return accountService.getProfile(id, token);
//    }

    @RequestMapping(path = "/profile", method = RequestMethod.GET, produces = "application/json")
    public Object showMyProfile() {
        return accountService.getMyProfile();
    }

    @RequestMapping(path = "/points/{value}", method = RequestMethod.POST, produces = "application/json")
    public Object addPoints(@PathVariable int value) {
        return accountService.addPoints(value);
    }

}

