package com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.controller;


import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.requests.NewMotoPlace;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.api.services.MotoPlacesService;
import com.MJ.MotoFreaksBackend.MotoFreaksBackend.config.consts.AuthKcConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/places")
public class MotoPlacesController {


    private final MotoPlacesService motoPlacesService;

    @Autowired
    public MotoPlacesController(MotoPlacesService motoPlacesService) {
        this.motoPlacesService = motoPlacesService;
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST, produces = "application/json")
    public Object addPost(HttpServletRequest req, @RequestBody NewMotoPlace newMotoPlace) {
        String token = req.getHeader(AuthKcConsts.HEADER_NAME).replace(AuthKcConsts.TOKEN_PREFIX, "");
//        return motoPlacesService.addPlace(newMotoPlace, token);
        return motoPlacesService.init(token);
    }


}
